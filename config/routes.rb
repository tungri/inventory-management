Inventory::Application.routes.draw do
  # get "asset/new"
  # get "asset/create"
  # get "sessions/login,"
  # get "sessions/home,"
  # get "sessions/profile,"
  # get "sessions/setting"
  # get "users/new"

  get ':controller(/:action(/:id))(.:format)'
  post ':controller(/:action(/:id))(.:format)'
  patch ':controller(/:action(/:id))(.:format)'
  root :to => "sessions#login"
  get "newAsset", :to => "assets#new"
  get "newOwner", :to => "owners#new"
  get "listOwners", :to => "owners#show"
  get "newVendor", :to => "vendors#new"
  get "listVendors", :to => "vendors#show"
  get "newEmployee", :to => "employees#new"
  get "listEmployees", :to => "employees#show"
  get "savePurchaseOrder", :to => "purchase_orders#create"

  get "signup", :to => "users#new"
  get "login", :to => "sessions#login"
  get "logout", :to => "sessions#logout"
  get "home", :to => "sessions#home"
  get "profile", :to => "sessions#profile"
  get "setting", :to => "sessions#setting" 
  post "users/create", :to => "users#create"
  post "assets/create", :to => "assets#create"
  post "owners/create", :to => "owners#create"
  post "sessions/login_attempt", :to => "sessions#login_attempt"
  post "owners/update", :to => "owners#update"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  # get 'users/create' => 'users#create'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  # match ':controller(/:action(/:id))(.:format)'
end
