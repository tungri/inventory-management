class ChangeAssignedToTableNameInAsset < ActiveRecord::Migration
  def up
    rename_column :assets, :assigned_to_id, :employee_id
  end

  def down
    rename_column :assets, :employee_id, :assigned_to_id
  end
end
