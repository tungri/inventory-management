class CreateAssignedTos < ActiveRecord::Migration
  def up
    create_table :assigned_tos do |t|
      t.string "name", :limit => 50, :null => false
      t.string "employee_code", :limit => 50, :null => false

      t.timestamps
    end
  end

  def down
    drop_table :assigned_tos
  end
end
