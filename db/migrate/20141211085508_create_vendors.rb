class CreateVendors < ActiveRecord::Migration
  def up
    create_table :vendors do |t|
      t.string "name", :limit => 50, :null => false
      t.string "manager", :limit => 70, :null => false
      t.string "address", :limit => 100, :default => '', :null => false
      t.integer "phone_no", :null => false
      t.integer "other_phone_no", :default => '', :null => false

      t.timestamps
    end
  end

  def down
    drop_table :vendors
  end
end
