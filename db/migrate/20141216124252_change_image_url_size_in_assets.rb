class ChangeImageUrlSizeInAssets < ActiveRecord::Migration
  def up
    change_column :assets, :image_url, :string
  end

  def down
    change_column :assets, :image_url, :string, :limit => 255
  end
end
