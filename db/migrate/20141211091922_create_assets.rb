class CreateAssets < ActiveRecord::Migration
  def up
    create_table :assets do |t|
      t.integer "user_id"
      t.integer "owner_id"
      t.integer "assigned_to_id"
      t.integer "purchase_order_id"
      t.integer "vendor_id"

      t.string "location", :limit => 50
      t.string "name", :limit => 50
      t.text "description"
      t.string "type", :limit => 50
      t.string "status", :limit => 50
      t.string "model_no", :limit => 100
      t.string "serial_no", :limit => 100
      t.string "mac_address", :limit => 100
      t.date "purchase_date"
      t.decimal "cost", :precision=>10, :scale=>2
      t.string "amc", :limit => 10
      t.decimal "amc_amount", :precision=>10, :scale=>2
      t.date "expiry_date"
      t.date "status_date"
      t.string "warranty_type", :limit => 50
      t.string "image_url", :limit => 100
      t.string "is_type", :limit => 50
      t.string "integrity", :limit => 10
      t.string "confidentiality", :limit => 10
      t.text "note"

      t.timestamps
    end
    add_index("assets","user_id")
    add_index("assets","owner_id")
    add_index("assets","assigned_to_id")
    add_index("assets","purchase_order_id")
    add_index("assets","vendor_id")
  end

  def down
    drop_table :assets
  end
end
