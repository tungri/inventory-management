class CreateOwners < ActiveRecord::Migration
  def up
    create_table :owners do |t|
      t.string "client", :null => false
      t.string "process", :default => '', :null => false
      t.string "name", :default => '', :null => false

      t.timestamps
    end
  end

  def down
    drop_table :owners
  end
end
