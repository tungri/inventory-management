class CreateAssetStatuses < ActiveRecord::Migration
  def up
    create_table :asset_statuses do |t|
      t.integer "asset_id"
      t.string "new_status", :null =>false

      t.timestamps
    end
    add_index("asset_statuses","asset_id")
  end

  def down
    drop_table :asset_statuses
  end
end
