class CreatePurchaseOrders < ActiveRecord::Migration
  def up
    create_table :purchase_orders do |t|
      t.string "name", :limit => 50, :null => false
      t.string "scanned_file_url", :limit => 100, :null => false

      t.timestamps
    end
  end

  def down
    drop_table :purchase_orders
  end
end
