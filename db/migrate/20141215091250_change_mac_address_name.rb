class ChangeMacAddressName < ActiveRecord::Migration
  def up
    rename_column("assets", "mac_ddress", "mac_address")
  end

  def down
    rename_column("assets", "mac_address", "mac_ddress")
  end
end
