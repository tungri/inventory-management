class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string "name", :limit => 50, :null => false
      t.string "email", :limit => 70, :null => false
      t.string "password", :limit => 100, :null => false
      t.string "salt"
      t.string "employee_code", :limit => 50, :null => false

      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end
