class ChangeEmployeeTable < ActiveRecord::Migration
  def up
    add_column :employees, :email, :string
    add_column :employees, :role, :string
  end

  def down
    remove_column :employees, :role
    remove_column :employees, :email
  end
end
