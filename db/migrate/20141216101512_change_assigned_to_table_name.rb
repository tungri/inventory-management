class ChangeAssignedToTableName < ActiveRecord::Migration
  def up
    rename_table :assigned_tos, :employees
  end

  def down
    rename_table :employees, :assigned_tos
  end
end
