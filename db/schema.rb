# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141224090054) do

  create_table "asset_statuses", force: true do |t|
    t.integer  "asset_id"
    t.string   "new_status", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "asset_statuses", ["asset_id"], name: "index_asset_statuses_on_asset_id", using: :btree

  create_table "assets", force: true do |t|
    t.integer  "user_id"
    t.integer  "owner_id"
    t.integer  "employee_id"
    t.integer  "purchase_order_id"
    t.integer  "vendor_id"
    t.string   "location",          limit: 50
    t.string   "name",              limit: 50
    t.text     "description"
    t.string   "asset_type",        limit: 50
    t.string   "status",            limit: 50
    t.string   "model_no",          limit: 100
    t.string   "serial_no",         limit: 100
    t.string   "mac_address",       limit: 100
    t.date     "purchase_date"
    t.decimal  "cost",                          precision: 10, scale: 2
    t.string   "amc",               limit: 10
    t.decimal  "amc_amount",                    precision: 10, scale: 2
    t.date     "expiry_date"
    t.date     "status_date"
    t.string   "warranty_type",     limit: 50
    t.string   "image_url"
    t.string   "is_type",           limit: 50
    t.string   "integrity",         limit: 10
    t.string   "confidentiality",   limit: 10
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "assets", ["employee_id"], name: "index_assets_on_employee_id", using: :btree
  add_index "assets", ["owner_id"], name: "index_assets_on_owner_id", using: :btree
  add_index "assets", ["purchase_order_id"], name: "index_assets_on_purchase_order_id", using: :btree
  add_index "assets", ["user_id"], name: "index_assets_on_user_id", using: :btree
  add_index "assets", ["vendor_id"], name: "index_assets_on_vendor_id", using: :btree

  create_table "employees", force: true do |t|
    t.string   "name",          limit: 50, null: false
    t.string   "employee_code", limit: 50, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.string   "role"
  end

  create_table "owners", force: true do |t|
    t.string   "client",                  null: false
    t.string   "process",    default: "", null: false
    t.string   "name",       default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_orders", force: true do |t|
    t.string   "name",             limit: 50,  null: false
    t.string   "scanned_file_url", limit: 100, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name",          limit: 50,  null: false
    t.string   "email",         limit: 70,  null: false
    t.string   "password",      limit: 100, null: false
    t.string   "salt"
    t.string   "employee_code", limit: 50,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vendors", force: true do |t|
    t.string   "name",           limit: 50,               null: false
    t.string   "manager",        limit: 70,               null: false
    t.string   "address",        limit: 100, default: "", null: false
    t.integer  "phone_no",                                null: false
    t.integer  "other_phone_no",             default: 0,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
