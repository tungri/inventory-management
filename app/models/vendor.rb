class Vendor < ActiveRecord::Base
  has_many :assets

  validates :name, :presence => true, :length => { :in => 1..200 }
  validates :manager, :presence => true, :length => { :in => 3..200 }
  validates :address, :presence => true, :length => { :in => 3..200 }
  validates :phone_no, :presence => true, :length => { :in => 3..200 }
  validates :other_phone_no, :length => { :in => 3..200 }
end
