class Asset < ActiveRecord::Base
  # include Tokenable

  belongs_to :owner
  belongs_to :vendor
  belongs_to :employee
  belongs_to :user
  belongs_to :purchase_order

  # before_save :get_unique_image_name

  # validates :image_url, :presence => true, :length => { :in => 3..200 }

  def self.save_asset_image asset_image
    asset_image_name = loop do
      random_token = SecureRandom.hex(5)
      break random_token unless Asset.exists?(image_url: random_token)
    end
    asset_image_name = asset_image_name.concat(".jpg")
    File.open(Rails.root.join('app','assets', 'images','upload', 'asset_images', asset_image_name), 'wb') do |file|
      file.write(asset_image.read)
    end
    image_url = ('/asset_images/'+ asset_image_name).to_s
  end

  def self.to_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
      end
    end
  end

end
