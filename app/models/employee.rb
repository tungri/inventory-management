class Employee < ActiveRecord::Base
  has_many :assets

  validates :name, :presence => true, :length => { :in => 3..200 }
  validates :employee_code, :presence => true, :length => { :in => 3..200 }
end
