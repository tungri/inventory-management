class PurchaseOrder < ActiveRecord::Base
  has_many :assets

  validates :name, :presence => true, :length => { :in => 3..200 }
  validates :scanned_file_url, :presence => true, :length => { :in => 3..200 }

  def self.save_purchase_order purchase_order
    purchase_order_name = loop do
      random_token = SecureRandom.hex(5)
      break random_token unless Asset.exists?(image_url: random_token)
    end
    purchase_order_name = purchase_order_name.concat(".jpg")
    File.open(Rails.root.join('app', 'assets', 'images','upload', 'purchase_orders', purchase_order_name), 'wb') do |file|
      file.write(purchase_order.read)
    end
    purchase_order = ('/purchase_orders/'+ purchase_order_name).to_s
  end
end
