class Owner < ActiveRecord::Base
  has_many :assets

  validates :client, :presence => true, :length => { :in => 3..200 }
  validates :process, :presence => true, :length => { :in => 3..200 }
  validates :name, :presence => true, :length => { :in => 3..200 }
end
