class VendorsController < ApplicationController
  respond_to :html, :js
  def new
    @vendor = Vendor.new
    render "newvendor"
  end

  def create
    @vendor = Vendor.new(vendor_params)
    respond_to do |format|
      if @vendor.save
        format.html { 
          flash[:notice] = "Vendor created successfully"
          flash[:color]= "valid"
          redirect_to :listVendors
         }
        format.js { render action: 'addtable', status: :created }
        # format.js { render js: "alert('successful');"}
      else
        flash[:notice] = "Form is invalid"
        flash[:color]= "invalid"
        format.html { render "newvendor" }
        format.js { render js: "alert('Error in the form');"}
      end
    end
  end

  def update
    @vendor = Vendor.find(params[:id])
    respond_to do |format|
      if @vendor.update_attributes(vendor_params)
        format.html {
          flash[:notice] = "You saved successfully"
          flash[:color]= "valid"
          redirect_to :listVendors
        }
        format.js { render action: 'updtable'}
      else
        flash[:notice] = "Form is invalid"
        flash[:color]= "invalid"
        format.html { render "newvendor" }
        format.js { render js: "alert('Error in the form');"}
      end
    end
  end

  def edit
    @vendor = Vendor.find(params[:id])
    respond_to do |format|
        # format.json { render :json => @asset.attributes.merge({ "owner" =>  owner}) }
        format.js {render action: 'editvendor'}
    end
  end

  def addtable
    render 'addtable.js.erb'
  end

  def updtable
  end

  def show
    @vendor = Vendor.new
    @vendors = Vendor.all
  end

  private
  def vendor_params
    params.require(:vendor).permit(:name, :manager, :address, :phone_no, :other_phone_no)
  end
end
