class SessionsController < ApplicationController

  before_filter :authenticate_user, :except => [:login, :login_attempt]
  before_filter :save_login_state, :only => [:login, :login_attempt]

  def login
    #Login Form
  end

  def login_attempt
    authorized_user = User.authenticate(params[:name_or_email],params[:login_password])
    if authorized_user
      session[:user_id] = authorized_user.id
      flash[:notice] = "Wow Welcome again, you logged in as #{authorized_user.name}"
      redirect_to(:action => 'home')
    else
      flash[:notice] = "Invalid Username or Password"
      flash[:color]= "invalid"
      render "login"  
    end
  end 

  def logout
    session[:user_id] = nil
    redirect_to :action => 'login'
  end 


  def home
    @owners = Owner.all
    @employees = Employee.all
    if params[:status] || params[:owner] || params[:employee] || params[:asset_type]
      # if params[:status].to_i!=0
      #   status = params[:status]
      # else
      #   status = 
      # end
      # if params[:asset_type] && params[:owner] && params[:owner].to_i!=0 && params[:asset_type]!='All'
      #   @assets = Asset.where({ asset_type: params[:asset_type], owner_id: params[:owner] })
      # elsif params[:asset_type] && params[:owner] && params[:owner].to_i==0 && params[:asset_type]!='All'
      #   @assets = Asset.where({ asset_type: params[:asset_type]})
      # elsif params[:asset_type] && params[:owner] && params[:owner].to_i!=0 && params[:asset_type]=='All'
      #   @assets = Asset.where({ owner_id: params[:owner]})
      # elsif params[:asset_type] && params[:owner] && params[:owner].to_i==0 && params[:asset_type]=='All'
      #   @assets = Asset.all
      # end
      if params[:asset_type]!='All'
        @assets = Asset.where asset_type: params[:asset_type]
      else
        @assets = Asset.all
      end
      if params[:owner].to_i!=0
        @assets = @assets.where owner_id: params[:owner]
      else
        @assets = @assets
      end
      if params[:status]!='All'
        @assets = Asset.where status: params[:status]
      else
        @assets = @assets
      end
      if params[:employee].to_i!=0
        @assets = @assets.where employee_id: params[:employee]
      else
        @assets = @assets
      end
    else
      @assets = Asset.all
    end
    # @image_url = Rails.root.join('app', 'uploads','image.jpg')
    # @owners = Owner.all
    # @vendors = Vendor.all
  end

  def profile
  end

  def setting
  end

  private
  def search_params
    params.permit(:asset_type, :owner, :status, :employee)
  end
end
