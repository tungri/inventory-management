class AssetsController < ApplicationController
  before_filter :authenticate_user, :only => [:create, :update]

  def new
    @asset = Asset.new
    @owners = Employee.order('name ASC').where role: 'Owner'
    # @assetstatuses = AssetStatus.where asset_id: @asset.id
    @vendors = Vendor.order('name ASC').all
    @employees = Employee.order('name ASC').all
  end

  def create
    @asset = Asset.new(asset_params)
    @asset.user_id = @current_user.id
    if params[:asset][:image]
      @asset.image_url = Asset.save_asset_image params[:asset][:image]
    end
    if params[:asset][:purchase_order]
      @purchase_order = PurchaseOrder.new
      @purchase_order.name = params[:asset][:purchase_order].original_filename
      @purchase_order.scanned_file_url = PurchaseOrder.save_purchase_order params[:asset][:purchase_order]
      if @purchase_order.save
        @asset.purchase_order_id = @purchase_order.id
      else
        flash[:notice] = "Form is invalid"
        flash[:color]= "invalid"
        @owners = Employee.order('name ASC').where role: 'Owner'
        @vendors = Vendor.all
        @employees = Employee.all
        render "new"
      end
    end
    if @asset.save
      @asset_status = AssetStatus.new
      @asset_status.asset_id = @asset.id
      @asset_status.new_status = @asset.status
      @asset_status.save
      flash[:notice] = "You signed up successfully"
      flash[:color]= "valid"
      redirect_to :home
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
      @owners = Employee.order('name ASC').where role: 'Owner'
      @vendors = Vendor.all
      @employees = Employee.all
      render "new"
    end
  end

  def update
    @asset = Asset.find(params[:id])
    if(@asset.status != params[:asset][:status]) 
      status_change = true
    else
      status_change = false
    end
    if params[:asset][:image]
      @asset.image_url = Asset.save_asset_image params[:asset][:image]
    end
    if params[:asset][:purchase_order]
      @purchase_order = PurchaseOrder.new
      @purchase_order.name = params[:asset][:purchase_order].original_filename
      @purchase_order.scanned_file_url = PurchaseOrder.save_purchase_order params[:asset][:purchase_order]
      if @purchase_order.save
        @asset.purchase_order_id = @purchase_order.id
      else
        flash[:notice] = "Form is invalid"
        flash[:color]= "invalid"
        @owners = Employee.order('name ASC').where role: 'Owner'
        @vendors = Vendor.all
        @employees = Employee.all
        render "new"
      end
    end
    if @asset.update_attributes(asset_params)
      if(status_change == true) 
        @asset_status = AssetStatus.new
        @asset_status.asset_id = @asset.id
        @asset_status.new_status = @asset.status
        @asset_status.save
      end
      flash[:notice] = "You saved successfully"
      flash[:color]= "valid"
      redirect_to :home
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
      @owners = Employee.order('name ASC').where role: 'Owner'
      @vendors = Vendor.all
      @employees = Employee.all
      render "new"
    end
  end

  def edit
    @asset = Asset.find(params[:id])
    @owners = Employee.order('name ASC').where role: 'Owner'
    @vendors = Vendor.all
    @employees = Employee.all
    # redirect_to :id => @asset.id
  end

  def show
    @asset = Asset.find(params[:id])
    # @assetId = params[:id]
    # owner = @asset.owner.name
    # render :json => @asset.attributes.merge({ "owner" =>  owner})
    respond_to do |format|
        # format.json { render :json => @asset.attributes.merge({ "owner" =>  owner}) }
        format.js {}
    end
    # respond_to do |format|
    #   format.js
    # end
    # render 'show'
  end

  def history
    @asset = Asset.find(params[:id])
    @assetstatuses = AssetStatus.where asset_id: @asset.id
    respond_to do |format|
        # format.json { render :json => @asset.attributes.merge({ "owner" =>  owner}) }
        format.js {}
    end
  end

  def export
    @assets = Asset.all
    respond_to do |format|
      format.csv do
        send_data @assets.to_csv
      end
    end
  end

  private
  def asset_params
    params.require(:asset).permit(:name, :location, :description, :asset_type, :owner_id, :employee_id, :status, :model_no, :serial_no, :mac_address, :purchase_date, :vendor_id, :cost, :amc, :amc_amount, :expiry_date, :status_date, :warranty_type, :is_type, :integrity, :confidentiality, :note)
  end
end
