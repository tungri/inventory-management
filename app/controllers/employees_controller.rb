class EmployeesController < ApplicationController
  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new(employee_params)
    if @employee.save
      flash[:notice] = "Owner created successfully"
      flash[:color]= "valid"
      redirect_to :listEmployees
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
      render "new"
    end
  end

  def update
    @employee = Employee.find(params[:id])
    # @owner = Owner.update(owner_params)
    if @employee.update_attributes(employee_params)
      flash[:notice] = "You saved successfully"
      flash[:color]= "valid"
      redirect_to :listEmployees
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
      render "new"
    end
  end

  def edit
    @employee = Employee.find(params[:id])
    respond_to do |format|
        # format.json { render :json => @asset.attributes.merge({ "owner" =>  owner}) }
        format.js {render action: 'editemployee'}
    end
  end

  def show
    @employees = Employee.all
  end

  private
  def employee_params
    params.require(:employee).permit(:name, :employee_code, :email, :role)
  end
end
