class OwnersController < ApplicationController
  def new
    @owner = Owner.new
  end

  def create
    @owner = Owner.new(owner_params)
    if @owner.save
      flash[:notice] = "Owner created successfully"
      flash[:color]= "valid"
      redirect_to :listOwners
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
      render "new"
    end
  end

  def update
    @owner = Owner.find(params[:id])
    # @owner = Owner.update(owner_params)
    if @owner.update_attributes(owner_params)
      flash[:notice] = "You saved successfully"
      flash[:color]= "valid"
      redirect_to :listOwners
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
      render "new"
    end
  end

  def edit
    @owner = Owner.find(params[:id])
  end

  def show
    @owners = Owner.all
  end

  private
  def owner_params
    params.require(:owner).permit(:client, :process, :name)
  end
end
